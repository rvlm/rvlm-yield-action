﻿using System;
using System.Collections;
using System.Text;

namespace Rvlm.YieldAction.IO
{
    public class YieldTextStream : IYieldTextInputStream, IYieldTextOutputStream
    {
        public IEnumerator Read(char[] buf, int index, int count)
        {
            throw new NotImplementedException();
        }

        public IEnumerator ReadLine(Ref<string> result)
        {
            throw new NotImplementedException();
        }
        
        public IEnumerator Write(char[] buf, int index, int count)
        {
            throw new NotImplementedException();
        }

        public IEnumerable Write(string str)
        {
            throw new NotImplementedException();
        }

        public IEnumerator WriteLine(string str)
        {
            throw new NotImplementedException();
        }
    }
}
