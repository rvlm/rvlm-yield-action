﻿using System;
using System.Collections;
using System.Text;

namespace Rvlm.YieldAction.IO
{
    public interface IYieldTextOutputStream
    {
        IEnumerator Write(char[] buf, int index, int count);

        IEnumerable Write(string str);

        IEnumerator WriteLine(string str);
    }
}
