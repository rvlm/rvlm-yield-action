﻿using System;
using System.Collections;
using System.Text;

namespace Rvlm.YieldAction.IO
{
    public interface IYieldBinaryInputStream
    {
        IEnumerator Read(byte buf, int offset, int size);
    }
}
