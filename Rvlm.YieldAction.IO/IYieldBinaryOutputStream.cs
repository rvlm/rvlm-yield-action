﻿using System;
using System.Collections;
using System.Text;

namespace Rvlm.YieldAction.IO
{
    public interface IYieldBinaryOutputStream
    {
        IEnumerator Write(byte buf, int offset, int size);
    }
}
