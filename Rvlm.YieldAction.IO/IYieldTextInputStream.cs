﻿using System;
using System.Collections;
using System.Text;

namespace Rvlm.YieldAction.IO
{
    public interface IYieldTextInputStream
    {
        IEnumerator Read(char[] buf, int index, int count);

        IEnumerator ReadLine(Ref<string> result);
    }
}
