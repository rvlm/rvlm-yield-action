﻿using System;

namespace Rvlm.YieldAction
{
    /**
     * Exception for yield invoker classes.
     * 
     * This exeption type is reserved for internal errors while dealing with yield
     * invokers and routines. It should be frown only in classes implementing
     * @c IYieldInvoker interface, and should never be thrown (and, maybe, caught) by end
     * user. It does not wrap any exeptions thrown from withing yield routines themselves,
     * so they must be handled separately. For example, the following code may lead to
     * throwing this exception, bacause of returning wrong result value:
     * @code
     *     IEnumerable<YieldResult> HelloAddams(IYieldInvoker invoker) {
     *         yield return (YieldResult)42;
     *     }
     * @end
     * 
     * @see IYieldInvoker
     * @see YieldInvoker
     */
    public class YieldException : Exception
    {
        public YieldException(string message)
            : base(message) {}

        public YieldException(string message, Exception innerException)
            : base(message, innerException) {}
    }
}
