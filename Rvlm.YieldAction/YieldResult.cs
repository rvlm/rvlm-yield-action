﻿using System;

namespace Rvlm.YieldAction
{
    /**
     * Intermediate result of yield subroutine.
     * 
     * This enumeration does not actually contain any constants because they are specific
     * to actual implementation. End user will never want to operate with this type,
     * the only one way he should use this enumeration is to declare yield subroutine
     * in his code, like below:
     * @code
     *     class Class {
     *         IEnumerable<YieldResult> routine(IYieldInvoker invoker, ... ) {
     *             ...
     *         }
     * @endcode
     * 
     * For stylistic purposes, one may want do declare an alias for
     * @c IEnumerable<YieldResult> with @c using statement, but C# currently limits
     * it to file scope.
     * 
     * @see IYieldInvoker
     * @see YieldInvoker
     */
    public enum YieldResult
    {
    }
}
