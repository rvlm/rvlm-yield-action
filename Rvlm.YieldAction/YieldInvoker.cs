﻿using System;
using System.Diagnostics;
using System.Collections.Generic;

namespace Rvlm.YieldAction
{
    using YieldEnumerator = IEnumerator<YieldResult>;

    /**
     * Controls yield subroutines execution and invokation.
     * 
     * @see IYieldInvoker
     * @see YieldResult
     * @see YieldRoutineDelegate
     */
    public class YieldInvoker : IYieldInvoker
    {
        // The constants below play the role of @c YieldResult enumeration items which
        // intentionally left blank for user not to operate on them on his own.
        private const int ResultDone   = 0;
        private const int ResultFail   = 1;
        private const int ResultEarly  = 2;
        private const int ResultInvoke = 3;

        /**
         * @internal
         * Call stack holding enumerator object for each yield subroutine called.
         * Incrementing those enumerators lead to advancing execution point to the next
         * yield of executing method.
         */
        private Stack<YieldEnumerator> stack;

        /** Constructs class object. */
        public YieldInvoker()
        {
            Ready  = true;
            Failed = false;
            stack  = new Stack<YieldEnumerator>();
        }

        /**
         * Returns @c true when invoker is ready for running new subroutine.
         * This means the old one is already finished, or no routine was run at all.
         * This property set to @c true even when subroutine fails.
         * @see Failed
         */
        public bool Ready { get; protected set; }

        /**
         * Returns @c true if current subroutine returns with @c IYieldInvoker.Fail.
         * @see Ready
         */
        public bool Failed { get; protected set; }

        /**
         * Resumes current subroutine.
         * @see Terminate
         */
        public void Resume()
        {
            if (stack.Count == 0)
                return;

            YieldEnumerator enumerator = stack.Peek();
            bool finished = !enumerator.MoveNext();

            // Treat 'yield break' situation the same as 'done' return.
            YieldResult result = finished
                ? (YieldResult)ResultDone
                : enumerator.Current;

            switch ((int)result)
            {
                case ResultDone:
                    enumerator = stack.Pop();
                    enumerator.Dispose();

                    Ready = (stack.Count == 0);
                    Failed = false;
                    break;

                case ResultFail:
                    Terminate();
                    break;

                case ResultEarly:
                    break;

                // TODO: Fix recursion.
                case ResultInvoke:
                    Resume();
                    break;

                default:
                    throw new YieldException("Yield routine returned unknown value.");
            }
        }

        /**
         * Forces current yield subroutine termination.
         * Also sets @c Failed property to @c false to distinguis from normal subroutine
         * termination. Invoker can start another subroutine right after this method is
         * called.
         */
        public void Terminate()
        {
            foreach (YieldEnumerator currentEnumerator in stack)
                currentEnumerator.Dispose();

            stack.Clear();
            Ready  = true;
            Failed = true;
        }

        /**
         *
         */
        #region Invoke(routine, ...)

        public void Invoke(YieldRoutineDelegate routine)
        {
            BeginEnumerator(routine(this).GetEnumerator());
        }

        public void Invoke<T1>(
            YieldRoutineDelegate<T1> routine, T1 arg1)
        {
            BeginEnumerator(routine(this, arg1).GetEnumerator());
        }

        public void Invoke<T1,T2>(
            YieldRoutineDelegate<T1,T2> routine, T1 arg1, T2 arg2)
        {
            BeginEnumerator(routine(this, arg1,arg2).GetEnumerator());
        }

        public void Invoke<T1,T2,T3>(
            YieldRoutineDelegate<T1,T2,T3> routine,
            T1 arg1, T2 arg2, T3 arg3)
        {
            BeginEnumerator(routine(this, arg1,arg2,arg3).GetEnumerator());
        }

        public void Invoke<T1,T2,T3,T4>(
            YieldRoutineDelegate<T1,T2,T3,T4> routine,
            T1 arg1, T2 arg2, T3 arg3, T4 arg4)
        {
            BeginEnumerator(routine(this, arg1,arg2,arg3,arg4).GetEnumerator());
        }

        public void Invoke<T1,T2,T3,T4,T5>(
            YieldRoutineDelegate<T1,T2,T3,T4,T5> routine,
            T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5)
        {
            BeginEnumerator(routine(this, arg1,arg2,arg3,arg4,arg5).GetEnumerator());
        }

        public void Invoke<T1,T2,T3,T4,T5,T6>(
            YieldRoutineDelegate<T1,T2,T3,T4,T5,T6> routine,
            T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6)
        {
            BeginEnumerator(
                routine(this, arg1,arg2,arg3,arg4,arg5,arg6).GetEnumerator());
        }

        public void Invoke<T1,T2,T3,T4,T5,T6,T7>(
            YieldRoutineDelegate<T1,T2,T3,T4,T5,T6,T7> routine,
            T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7)
        {
            BeginEnumerator(
                routine(this, arg1,arg2,arg3,arg4,arg5,arg6,arg7).GetEnumerator());
        }

        #endregion

        YieldResult IYieldInvoker.Done()
        {
            return (YieldResult)ResultDone;
        }

        YieldResult IYieldInvoker.Fail()
        {
            return (YieldResult)ResultFail;
        }

        YieldResult IYieldInvoker.Early()
        {
            return (YieldResult)ResultEarly;
        }

        YieldResult IYieldInvoker.Pause(int delay)
        {
            return ((IYieldInvoker)this).Invoke(WaitRoutine, delay);
        }

        #region IYieldInvoker.Invoke(subroutine, ...)

        YieldResult IYieldInvoker.Invoke(
            YieldRoutineDelegate subroutine)
        {
            return InvokeEnumerator(subroutine(this).GetEnumerator());
        }

        YieldResult IYieldInvoker.Invoke<T1>(
            YieldRoutineDelegate<T1> subroutine, T1 arg1)
        {
            return InvokeEnumerator(subroutine(this, arg1).GetEnumerator());
        }

        YieldResult IYieldInvoker.Invoke<T1,T2>(
            YieldRoutineDelegate<T1,T2> subroutine, T1 arg1, T2 arg2)
        {
            return InvokeEnumerator(subroutine(this, arg1, arg2).GetEnumerator());
        }

        YieldResult IYieldInvoker.Invoke<T1,T2,T3>(
            YieldRoutineDelegate<T1,T2,T3> subroutine, T1 arg1, T2 arg2, T3 arg3)
        {
            return InvokeEnumerator(subroutine(this, arg1,arg2,arg3).GetEnumerator());
        }

        YieldResult IYieldInvoker.Invoke<T1,T2,T3,T4>(
            YieldRoutineDelegate<T1,T2,T3,T4> subroutine,
            T1 arg1, T2 arg2, T3 arg3, T4 arg4)
        {
            return InvokeEnumerator(
                subroutine(this, arg1,arg2,arg3,arg4).GetEnumerator());
        }

        YieldResult IYieldInvoker.Invoke<T1,T2,T3,T4,T5>(
            YieldRoutineDelegate<T1,T2,T3,T4,T5> subroutine,
            T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5)
        {
            return InvokeEnumerator(
                subroutine(this, arg1,arg2,arg3,arg4,arg5).GetEnumerator());
        }

        YieldResult IYieldInvoker.Invoke<T1,T2,T3,T4,T5,T6>(
            YieldRoutineDelegate<T1,T2,T3,T4,T5,T6> another,
            T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6)
        {
            return InvokeEnumerator(
                another(this, arg1,arg2,arg3,arg4,arg5,arg6).GetEnumerator());
        }

        YieldResult IYieldInvoker.Invoke<T1,T2,T3,T4,T5,T6,T7>(
            YieldRoutineDelegate<T1,T2,T3,T4,T5,T6,T7> subroutine,
            T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7)
        {
            return InvokeEnumerator(
                subroutine(this, arg1,arg2,arg3,arg4,arg5,arg6,arg7).GetEnumerator());
        }

        #endregion

        /**
         * @internal
         * 
         */
        protected void BeginEnumerator(YieldEnumerator enumerator)
        {
            Ready  = false;
            Failed = false;
            stack.Push(enumerator);
            Resume();
        }

        /**
         * @internal
         *
         */
        protected YieldResult InvokeEnumerator(YieldEnumerator enumerator)
        {
            stack.Push(enumerator);
            return (YieldResult)ResultInvoke;
        }

        /**
         * @internal
         * Yield subroutine for waiting in @c Pause method.
         * Does nothing but yields with 'early' state until @a delay milliseconds passed.
         * After that exits with 'done' state.
         */
        protected static IEnumerable<YieldResult> WaitRoutine(
            IYieldInvoker invoker, int delay)
        {
            Stopwatch watch = new Stopwatch();
            watch.Start();

            while (watch.ElapsedMilliseconds < delay)
            {
                yield return invoker.Early();
            }

            watch.Reset();
            yield return invoker.Done();
        }
    }
}
