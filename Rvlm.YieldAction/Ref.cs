﻿using System;

namespace Rvlm.YieldAction
{
    public struct Ref<T>
    {
        public T Value;
    }
}
