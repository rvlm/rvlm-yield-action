﻿using System;

namespace Rvlm.YieldAction
{
    /**
     * Yield invoker abstraction interface.
     * 
     * This interface describes methods which must be supported by any yield invoker
     * implementation. It contains only methods which are required within yield procedure,
     * with the bare minimum of features. To provide yield routines with ability to be
     * invoked using any specific invoker implementation, the actual invoker is passed
     * to them as @c IYieldInvoker.
     *
     * This library provides @c YieldInvoker class which is the default implementation
     * of this interface using stack-based approach.
     * 
     * @see YieldInvoker
     */
    public interface IYieldInvoker
    {
        /**
         * Sucessfully ends current subroutine.
         * Control flow returns to the caller subroutine. The <tt>yield break</tt>
         * statement may be also used for this purpose, too. See usage example below:
         * @code
         *     IEnumerable<YieldResult> routine(IYieldInvoker invoker)
         *     {
         *         yield return invoker.Invoke(subroutine);
         *     }
         * 
         *     IEnumerable<YieldResult> subroutine(IYieldInvoker invoker)
         *     {
         *         yield return invoker.Done();
         *         // Control flow will never go below, it will return
         *         // to routine() instead.
         *     }
         * @endcode
         * @see Fail
         * @see Early
         */
        YieldResult Done();

        /**
         * Ends current subroutine with failure.
         * Control flow doesn't return to the caller subroutine. The whole call stack
         * is cleared instead, and a failure flag of invoker is set (which is specific 
         * to implementation). This method should be used with care and only when
         * unrecoverable error occurs. See example usage below:
         * @code
         *     IEnumerable<YieldResult> routine(IYieldInvoker invoker)
         *     {
         *         yield return invoker.Invoke(subroutine);
         *         // Control flow will never return here.
         *     }
         * 
         *     IEnumerable<YieldResult> subroutine(IYieldInvoker invoker)
         *     {
         *         yield return invoker.Fail();
         *         // Control flow will not reach here.
         *     }
         * @endcode
         * @see Done
         * @see Early
         * @see YieldInvoker.IsFailed
         */
        YieldResult Fail();

        /**
         * Yields control flow because current subroutine cannot continue execution yet.
         * Using this method will allow program to procede with another subroutine while
         * the current one is waiting for some event (e.g., I/O completion). See usage
         * example below which is a simplified method for reading data from a serial port:
         * @code
         *     IEnumerable<YieldResult> readBytes(
         *         IYieldInvoker invoker, SerialPort port, byte[] buffer, int count) 
         *     {
         *         int read = 0;
         *         while (read < count) {
         *            if (port.BytesAvailable <= 0) yield return invoker.Early();
         *            else {
         *                int toRead = Math.Min(port.BytesAvailable, count - read);
         *                port.Read(buffer, read, toRead);
         *            }
         *         }
         *         yield return invoker.Done();
         *     }
         * @endcode
         * @see Done
         * @see Fail
         */
        YieldResult Early();

        /**
         * Pauses current subroutine for some time.
         * This method does not freeze current thread, instead it returns control flow
         * back with @c Early() method. The pause duration is given in milliseconds,
         * but it can be only guaranteed that the pause will be <em>at least</em> that
         * long. The example below is the simplified polling method of hypothetic (and,
         * possibly, quite buggy) device:
         * @code
         *     IEnumerable<YieldResult> pollDevice(IYieldInvoker invoker)
         *     {
         *         while (true) {
         *             yield return invoker.Invoke(sendRequest);
         *             yield return invoker.Invoke(recvResponse);
         *             
         *             // Wait for a while, or we can break the device if ask
         *             // for it too often.
         *             yield return invoker.Pause(500);
         *         }
         *     }
         * @endcode
         * @see Early
         */
        YieldResult Pause(int milliseconds);

        /**
         * Call nested subroutine.
         * The subroutine to call is passed as @a subroutine parameter, with the rest
         * of parameters be its arguments. After nested subroutine ends (with @c Done,
         * <tt>yield break</tt> or when control flow reaches its end), the caller routine
         * execution is continued as ordinary.
         * @see YieldRoutine
         * @see YieldInvoker
         */
        ///@{
        YieldResult Invoke(YieldRoutineDelegate subroutine);

        YieldResult Invoke<T1>(YieldRoutineDelegate<T1> subroutine, T1 arg1);

        YieldResult Invoke<T1,T2>(
            YieldRoutineDelegate<T1,T2> subroutine,
            T1 arg1, T2 arg2);

        YieldResult Invoke<T1,T2,T3>(
            YieldRoutineDelegate<T1,T2,T3> subroutine,
            T1 arg1, T2 arg2, T3 arg3);

        YieldResult Invoke<T1,T2,T3,T4>(
            YieldRoutineDelegate<T1,T2,T3,T4> subroutine,
            T1 arg1, T2 arg2, T3 arg3, T4 arg4);

        YieldResult Invoke<T1,T2,T3,T4,T5>(
            YieldRoutineDelegate<T1,T2,T3,T4,T5> subroutine,
            T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5);

        YieldResult Invoke<T1,T2,T3,T4,T5,T6>(
            YieldRoutineDelegate<T1,T2,T3,T4,T5,T6> subroutine,
            T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6);

        YieldResult Invoke<T1,T2,T3,T4,T5,T6,T7>(
            YieldRoutineDelegate<T1,T2,T3,T4,T5,T6,T7> subroutine,
            T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7);
        ///@}
    }
}
