﻿using System;
using System.Collections.Generic;

namespace Rvlm.YieldAction
{
    /**
     * Yield subroutines delegates types.
     * 
     * This denotes a method which can be used in conjunction with @c YieldInvoker,
     * metho must meet the following requirements
     * on its parameters and return value:
     * 
     * - Return type must be @c IEnumerable<YieldResult>, because C# allows using of
     *   <tt>yield return</tt> only in methods returning @c IEnumerable<T>.
     * - First parameter must be @c IYieldInvoker, which is used to control the control
     *   flow and calling other subroutines in convenient way.
     * - Other parameters (up to seven) may be of any type and may not have @c ref
     *   or @c out qualifiers. The maximum number of arguments was selected arbitrary.
     * 
     * Yield subroutines may throw any exception they need which may be caught by ordinary
     * @c catch clause, but they must not throw @c YieldException by themselves, because
     * this exception type is reserved for use by @c YieldInvoker.
     * 
     * @see IYieldInvoker
     * @see YieldInvoker
     * @see YieldResult
     */
    ///@{
    public delegate IEnumerable<YieldResult>
        YieldRoutineDelegate(IYieldInvoker invoker);

    public delegate IEnumerable<YieldResult>
        YieldRoutineDelegate<T1>(IYieldInvoker invoker, T1 arg1);

    public delegate IEnumerable<YieldResult>
        YieldRoutineDelegate<T1,T2>(IYieldInvoker invoker, T1 arg1, T2 arg);

    public delegate IEnumerable<YieldResult>
        YieldRoutineDelegate<T1,T2,T3>(IYieldInvoker invoker, T1 arg1, T2 arg2, T3 arg3);

    public delegate IEnumerable<YieldResult>
        YieldRoutineDelegate<T1,T2,T3,T4>(IYieldInvoker invoker,
            T1 arg1, T2 arg2, T3 arg3, T4 arg4);

    public delegate IEnumerable<YieldResult>
        YieldRoutineDelegate<T1,T2,T3,T4,T5>(IYieldInvoker invoker,
            T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5);

    public delegate IEnumerable<YieldResult>
        YieldRoutineDelegate<T1,T2,T3,T4,T5,T6>(IYieldInvoker invoker,
            T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6);

    public delegate IEnumerable<YieldResult>
        YieldRoutineDelegate<T1,T2,T3,T4,T5,T6,T7>(IYieldInvoker invoker,
            T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7);
    ///@}
}
