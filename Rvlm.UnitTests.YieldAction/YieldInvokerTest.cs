﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Rvlm.YieldAction;
using NUnit.Framework;

namespace Test.HMI.YieldAction
{
    using Yield = IEnumerable<YieldResult>;

    [TestFixture]
    public class YieldInvokerTest
    {
        public Yield mainRoutine(IYieldInvoker invoker, List<string> trace)
        {
            trace.Add("ENTER mainRoutine");
            yield return invoker.Invoke(subRoutine1, trace);
            yield return invoker.Invoke(subRoutine2, trace);
            trace.Add("LEAVE mainRoutine");
        }

        public Yield subRoutine1(IYieldInvoker invoker, List<string> trace)
        {
            trace.Add("ENTER subRoutine1");
            yield return invoker.Invoke(subRoutine2, trace);
            trace.Add("LEAVE subRoutine1");
        }

        public Yield subRoutine2(IYieldInvoker invoker, List<string> trace)
        {
            trace.Add("ENTER subRoutine2");
            trace.Add("LEAVE subRoutine2");
            yield return invoker.Done();
        }

        public Yield recursiveRoutine(IYieldInvoker invoker, List<int> trace, int num)
        {
            trace.Add(num);
            yield return (num > 0)
                ? invoker.Invoke(recursiveRoutine, trace, num-1)
                : invoker.Done();
        }

        public Yield failingRoutine(
            IYieldInvoker invoker, List<int> trace, int num, int failNum)
        {
            trace.Add(num);
            yield return (num > 0 && num != failNum)
                ? invoker.Invoke(failingRoutine, trace, num - 1, failNum)
                : invoker.Fail();
        }

        Yield pausingRoutine(IYieldInvoker invoker, int milliseconds)
        {
            yield return invoker.Pause(milliseconds);
        }

        [Test]
        public void NonRecursiveRoutines()
        {
            YieldInvoker invoker = new YieldInvoker();
            List<string> traceValues = new List<string>();

            invoker.Invoke(mainRoutine, traceValues);
            while (!invoker.Ready)
                invoker.Resume();

            Assert.AreEqual(8, traceValues.Count);
            Assert.AreEqual("ENTER mainRoutine", traceValues[0]);
            Assert.AreEqual("ENTER subRoutine1", traceValues[1]);
            Assert.AreEqual("ENTER subRoutine2", traceValues[2]);
            Assert.AreEqual("LEAVE subRoutine2", traceValues[3]);
            Assert.AreEqual("LEAVE subRoutine1", traceValues[4]);
            Assert.AreEqual("ENTER subRoutine2", traceValues[5]);
            Assert.AreEqual("LEAVE subRoutine2", traceValues[6]);
            Assert.AreEqual("LEAVE mainRoutine", traceValues[7]);
        }

        [Test]
        public void RecursiveRoutine()
        {
            YieldInvoker invoker = new YieldInvoker();
            List<int> trace = new List<int>();

            const int recursionDepth = 1024;
            invoker.Invoke(recursiveRoutine, trace, recursionDepth);
            while (!invoker.Ready)
                invoker.Resume();

            Assert.AreEqual(recursionDepth+1, trace.Count);

            int expectedNum = recursionDepth;
            foreach (int actualNum in trace)
            {
                Assert.AreEqual(expectedNum, actualNum);
                expectedNum--;
            }
        }

        [Test]
        public void TerminatingRoutine()
        {
            YieldInvoker invoker = new YieldInvoker();
            List<int> trace = new List<int>();
            invoker.Invoke(recursiveRoutine, trace, 1024);
            invoker.Resume();
            invoker.Resume();
            invoker.Resume();
            invoker.Terminate();
        }

        [Test]
        public void FailingRoutine()
        {
            YieldInvoker invoker = new YieldInvoker();
            List<int> trace = new List<int>();

            const int recursionDepth = 1024;
            const int failingDepth = 42;
            invoker.Invoke(failingRoutine, trace, recursionDepth, failingDepth);
            while (!invoker.Ready)
                invoker.Resume();

            Assert.AreEqual(recursionDepth - failingDepth + 1, trace.Count);

            int expectedNum = recursionDepth;
            foreach (int actualNum in trace)
            {
                Assert.AreEqual(expectedNum, actualNum);
                Assert.GreaterOrEqual(expectedNum, failingDepth);
                expectedNum--;
            }

            Assert.IsTrue(invoker.Ready);
            Assert.IsTrue(invoker.Failed);
        }

        [Test]
        [Explicit]
        public void _MeasureOverheadTime()
        {
            YieldInvoker invoker = new YieldInvoker();
            Stopwatch pauseStopwatch = new Stopwatch();

            const int MeasureCount = 20000;
            pauseStopwatch.Start();
            for (int i = 0; i < MeasureCount; i++)
            {
                invoker.Invoke(pausingRoutine, 0);
                while (!invoker.Ready)
                {
                    invoker.Resume();
                }
            }
            pauseStopwatch.Stop();
            Assert.Fail(
                string.Format(
                    "Average yield-invoker overhead: {0:F4} milliseconds (in {1} runs).",
                    pauseStopwatch.ElapsedMilliseconds / (double)MeasureCount,
                    MeasureCount));
        }

        [Test]
        public void PausingRoutine()
        {
            YieldInvoker invoker = new YieldInvoker();
            Stopwatch pauseStopwatch = new Stopwatch();

            int requestedTime = 1000;
            pauseStopwatch.Start();
            invoker.Invoke(pausingRoutine, requestedTime);
            while (!invoker.Ready)
            {
                invoker.Resume();
            }
            pauseStopwatch.Stop();

            int actualTime = (int)pauseStopwatch.ElapsedMilliseconds;
            Assert.GreaterOrEqual(actualTime, requestedTime);
            Assert.LessOrEqual(actualTime, requestedTime+requestedTime/10);
        }

        [Test]
        public void PausingRoutineWithNegativeTimeout()
        {
            YieldInvoker invoker = new YieldInvoker();
            invoker.Invoke(pausingRoutine, -42000);
            while (!invoker.Ready)
            {
                invoker.Resume();
            }
        }
    }
}
